package com.calculatot.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.calculatot.exception.InvalidDataException;
import com.calculatot.model.Health;

/**
 * @author akshay
 * This class will return the amount
 * based on the health conditions
 *
 */
public class CalculateAmtOnHealth {
	
	/**
	 * @param health
	 * @param basePremium
	 * @return
	 * @throws InvalidDataException 
	 */
	
	public static Map getHealthCondition() throws InvalidDataException {
	
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Health details");
		System.out.println("HyperTension");
		String hypten = scan.next();
		if(!(hypten.equalsIgnoreCase("yes") || (hypten.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		System.out.println("Blood Pressure");
		String bp = scan.next();
		if(!(bp.equalsIgnoreCase("yes") || (bp.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		System.out.println("Blood Sugar");
		String sugar = scan.next();
		if(!(sugar.equalsIgnoreCase("yes") || (sugar.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		System.out.println("Over Weight");
		String ow = scan.next();
		if(!(ow.equalsIgnoreCase("yes") || (ow.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		
		Map<String, String> healthMap = new HashMap<String, String>();
		healthMap.put("HypTen", hypten);
		healthMap.put("BloPre", bp);
		healthMap.put("BloSug", sugar);
		healthMap.put("OvrWgt", ow);
		
		return healthMap;
	}
	public static double premiumAmountOnHealth( double basePremium, Map healthMap) throws InvalidDataException{
		double healthAmt = 0;
		
		double hypTenAmt = 0;
		double bloodPreAmt = 0;
		double bloodSugAmt = 0;
		double overweight = 0;
		if(((String) healthMap.get("HypTen")).equalsIgnoreCase("yes")){
			hypTenAmt = basePremium*0.01;
		}if(((String) healthMap.get("BloPre")).equalsIgnoreCase("yes")){
			bloodPreAmt = basePremium*0.01;
		}if(((String) healthMap.get("BloSug")).equalsIgnoreCase("yes")){
			bloodSugAmt = basePremium*0.01;
		}if(((String) healthMap.get("OvrWgt")).equalsIgnoreCase("yes")){
			overweight = basePremium*0.01;
		}
		
		healthAmt = (hypTenAmt+bloodPreAmt+bloodSugAmt+overweight);
		return healthAmt;
		
	}
	
	

}
