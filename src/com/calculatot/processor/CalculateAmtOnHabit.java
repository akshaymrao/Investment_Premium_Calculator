package com.calculatot.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.calculatot.exception.InvalidDataException;
import com.calculatot.model.Habits;

public class CalculateAmtOnHabit {
	
	/**
	 * @param habits
	 * @param basePremium
	 * @return
	 * @throws InvalidDataException 
	 */
	
	public static Map getHabits() throws InvalidDataException {
		Habits habits = null;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Habits");
		System.out.println("Smoking");
		String smoking = scan.next();
		if(!(smoking.equalsIgnoreCase("yes") || (smoking.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		System.out.println("Alcohol");
		String alcohol = scan.next();
		if(!(alcohol.equalsIgnoreCase("yes") || (alcohol.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		System.out.println("Excercise");
		String exercise = scan.next();
		if(!(exercise.equalsIgnoreCase("yes") || (exercise.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		System.out.println("Drugs");
		String drugs = scan.next();
		if(!(drugs.equalsIgnoreCase("yes") || (drugs.equalsIgnoreCase("no")))){
			throw new InvalidDataException();
		}
		
		
		Map<String, String> habitsMap = new HashMap<String, String>();
		habitsMap.put("Smoking", smoking);
		habitsMap.put("Alcohol", alcohol);
		habitsMap.put("Excercise", exercise);
		habitsMap.put("Drugs", drugs);
		

		return habitsMap;
	}
	public static double premiumAmountOnHabit(double basePremium, Map habitsMap) throws InvalidDataException{
		
		CalculateAmtOnAge calAge = new CalculateAmtOnAge();
		CalculateAmtOnHabit habitPremium = new CalculateAmtOnHabit();
		CalculateAmtOnHealth healthPremium = new CalculateAmtOnHealth();
		AmountCalculator amountCalculator = new AmountCalculator();
		
		double goodHabitsAmt = 0;
		
		double smokingAmt = 0;
		double alcoholAmt = 0;
		double exerAmt = 0;
		double drugsAmt = 0;
	
		if(((String) habitsMap.get("Smoking")).equalsIgnoreCase("yes")){
			 smokingAmt = basePremium*0.03;
			}if(((String) habitsMap.get("Alcohol")).equalsIgnoreCase("yes")){
				 alcoholAmt = basePremium*0.03;
				}if(((String) habitsMap.get("Excercise")).equalsIgnoreCase("yes")){
					exerAmt = basePremium*0.03;
				}if(((String) habitsMap.get("Drugs")).equalsIgnoreCase("yes")){
					 drugsAmt = basePremium*0.03;
				}
				goodHabitsAmt = smokingAmt+alcoholAmt-exerAmt+drugsAmt;
		
		return goodHabitsAmt;
		
	}

}
