package com.calculatot.processor;

import java.util.Map;
import java.util.Scanner;
import com.calculatot.exception.InvalidDataException;
import com.calculatot.model.Habits;
import com.calculatot.model.Health;

public class InvestmentCalculator {

	/**
	 * @param args
	 * @throws InvalidDataException
	 */
	public static void main(String[] args) throws InvalidDataException {
		// TODO Auto-generated method stub
		double basePremium = 5000;

		/** taking input from user **/

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the name");
		String name = scan.next();

		AmountCalculator amtCalc = new AmountCalculator();
		double finalPremiumAmount = amtCalc.returnAmount(basePremium);
		
		/** final output **/
		System.out.println("Final Premium Amount is " + Math.round(finalPremiumAmount));
	}

}
