package com.calculatot.processor;

import java.util.Scanner;

import com.calculatot.exception.InvalidDataException;

public class CalculateAmountOnGender {
	
	public static String getGender() throws InvalidDataException {
		
		System.out.println("enter gender");
		Scanner scan = new Scanner(System.in);
		String gender = scan.next();
		if(!((gender.equalsIgnoreCase("male")) || (gender.equalsIgnoreCase("female")) || (gender.equalsIgnoreCase("others")))){
			throw new InvalidDataException();
		}
		
		
		return gender;
	}
	
	public static double premiumAmountOnGender(double basePremium) throws InvalidDataException{
		
		String gender = getGender();
		
		double malePremAmt = basePremium*0.02;
		double femalePremAmt = basePremium*0.04;
		double otherPremAmt = basePremium*0.06;
		double premiumAmtGen = 0;
		if(gender.equalsIgnoreCase("male")){
			premiumAmtGen = malePremAmt;
		}else if(gender.equalsIgnoreCase("female")){
			premiumAmtGen = femalePremAmt;
		}else if(gender.equalsIgnoreCase("Others")){
			premiumAmtGen = otherPremAmt;
		}
		return premiumAmtGen;
		
	}

}
