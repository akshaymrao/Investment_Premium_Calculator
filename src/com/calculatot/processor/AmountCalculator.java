package com.calculatot.processor;

import java.util.Map;

import com.calculatot.exception.InvalidDataException;

/**
 * @author akshay
 * This class will calculate the 
 * total amount based on
 * the condition
 *
 */

public class AmountCalculator {
	
	/**
	 * @param premiumAmtage
	 * @param premiumAmtGen
	 * @param goodHabitsAmt
	 * @param healthAmt
	 * @return
	 */
	public static double returnAmount(double basePremium) throws InvalidDataException{
		double finalPremiumAmount = 0;
		CalculateAmtOnAge calAge = new CalculateAmtOnAge();
		CalculateAmountOnGender calGen = new CalculateAmountOnGender();
		CalculateAmtOnHabit habitPremium = new CalculateAmtOnHabit();
		CalculateAmtOnHealth healthPremium = new CalculateAmtOnHealth();
		AmountCalculator amountCalculator = new AmountCalculator();
		
		
		double amt, premiumAmtage, premiumAmtGen, totAmt, goodHabitsAmt, healthAmt = 0;
		premiumAmtage = calAge.premiumAmountOnAge(basePremium);
		premiumAmtGen = calGen.premiumAmountOnGender(premiumAmtage);
		totAmt = (premiumAmtage + premiumAmtGen);
		Map habitsMap = habitPremium.getHabits();
		Map healthMap = healthPremium.getHealthCondition();
		goodHabitsAmt = habitPremium.premiumAmountOnHabit(totAmt, habitsMap);
		healthAmt = healthPremium.premiumAmountOnHealth(totAmt, healthMap);
		finalPremiumAmount = amountCalculator.returnFinalAmount(premiumAmtage, premiumAmtGen, goodHabitsAmt,healthAmt);
			
		
		return finalPremiumAmount;
	}
	
	public static double returnFinalAmount(double premiumAmtage, double premiumAmtGen, double goodHabitsAmt, double healthAmt){
		double finalPremiumAmount = 0;
		
		finalPremiumAmount = (premiumAmtage+premiumAmtGen+healthAmt+goodHabitsAmt);
		return finalPremiumAmount;
	}
}
