package com.calculatot.processor;

import java.util.Scanner;

import com.calculatot.exception.InvalidDataException;

/**
 * class will return amount
 * based on age and gender
 */
public class CalculateAmtOnAge {
	
	public static int inputAge() {
		
		System.out.println("Enter the age");
		Scanner scan = new Scanner(System.in);
		int age = scan.nextInt();
		
		
		return age;
		
	}
	
	
	public static double premiumAmountOnAge (double basePremium){
		
		int age = inputAge();
		
		double premiumSlabOne = basePremium+basePremium*0.1;
		double premimumSlabTwo = premiumSlabOne+premiumSlabOne*0.1;
		double premiumSlabThree = premimumSlabTwo+premimumSlabTwo*0.1;
		double premiumSlabFour = premiumSlabThree+premiumSlabThree*0.1;
		double premiumAmtage = 0;
		if(age >= 18 && age <= 25){
			premiumAmtage = premiumSlabOne;
		}else if(age >= 25 && age <= 30){
			premiumAmtage = premimumSlabTwo;
		}else if(age > 30 && age < 35){
			premiumAmtage = premiumSlabThree;
		}else if(age >= 35 && age <= 40){
			premiumAmtage = premiumSlabFour;
		}else if(age > 40){
			int ageSlab = age - 40;
			while(ageSlab >= 5){
				premiumAmtage = premiumSlabFour*0.2;
				ageSlab = ageSlab - 5;
			}
			
		}
		return premiumAmtage;
		
	}
	
	
}
