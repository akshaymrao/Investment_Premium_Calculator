package com.calculatot.model;

public class Health {
	
	private String hyperTension;
	private String bloodPressure;
	private String bloodSugar;
	private String overWeight;
	public String getHyperTension() {
		return hyperTension;
	}
	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public String getOverWeight() {
		return overWeight;
	}
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}
	@Override
	public String toString() {
		return "Health [hyperTension=" + hyperTension + ", bloodPressure=" + bloodPressure + ", bloodSugar="
				+ bloodSugar + ", overWeight=" + overWeight + "]";
	}
	
	

}
