package com.calculatot.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PersonData {
	
	private String name;
	private int age;
	private String gender;
	
	Set<Health> health = new HashSet();
	Set<Habits> habits = new HashSet();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Set<Health> getHealth() {
		return health;
	}
	public void setHealth(Set<Health> health) {
		this.health = health;
	}
	public Set<Habits> getHabits() {
		return habits;
	}
	public void setHabits(Set<Habits> habits) {
		this.habits = habits;
	}
	@Override
	public String toString() {
		return "PersonData [name=" + name + ", age=" + age + ", gender=" + gender + ", health=" + health + ", habits="
				+ habits + "]";
	}
	
	
}
