package com.calculatot.junit;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import com.calculatot.exception.InvalidDataException;
import com.calculatot.processor.AmountCalculator;
import com.calculatot.processor.CalculateAmtOnAge;
import com.calculatot.processor.CalculateAmtOnHabit;

public class PremiumCalculatorJunit {
	
	@Test
	public void total(){
		AmountCalculator amtCal = new AmountCalculator();
		double act = amtCal.returnFinalAmount(6655.0, 133.1, 0.0, 67.881);
		double exp = 6856.99;
		double delta = 0;
		assertEquals(act, act, delta);
	}
	
	@Test
	public void genderAmt() throws InvalidDataException {
		CalculateAmtOnAge calAge = new CalculateAmtOnAge();
		double act = CalculateAmtOnAge.premiumAmountOnAge(45);
		double exp = 5500;
		double delta = 0;
		assertEquals(act, exp, delta);
	}
	
	
	@Test
	public void amtAge(){
		CalculateAmtOnAge ageCal = new CalculateAmtOnAge();
		double act = ageCal.premiumAmountOnAge(34);
		double exp = 6655;
		double delta = 0;
		assertEquals(act, act, delta);
	}
}

